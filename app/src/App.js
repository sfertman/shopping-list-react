import { useEffect, useState } from "react";
// import { LocalClient as Client } from "./client";
import { Client } from "./client";
import { catValues, dissoc, groupBy, mapValues, sortEntriesByKey } from "./util";

const STATUS = { NEW: 0, DONE: 1 };

const egData = {// example data -- this will be fetched from backend
  "Hit the gym": { status: STATUS.NEW },
  "Pay bills": { status: STATUS.DONE },
  "Meet George": { status: STATUS.NEW },
  "Buy eggs": { status: STATUS.NEW },
  "Read a book": { status: STATUS.NEW },
  "Organize office": { status: STATUS.NEW }
}

export default function App() {

  function Item({ item }) {
    const [title, { status }] = item;
    function toggleDone() {
      if (status == STATUS.NEW) {
        setItems({ ...items, [title]: { status: STATUS.DONE } });
      } else {
        setItems({ ...items, [title]: { status: STATUS.NEW } });
      }
    }

    function rmItem() {
      setItems(dissoc(items, title));
    }

    const newStyle = "bg-slate-200 hover:bg-slate-300 odd:bg-white text-slate-950"
    const doneStyle = "bg-slate-400 hover:bg-slate-500 text-white"
    const isDone = status == STATUS.DONE;
    return (
      <li className={`border-y-[0.5px] text-lg cursor-pointer relative select-none ${isDone ? doneStyle : newStyle}`}>
        <div className="flex flex-row">
          <div className={`flex grow py-[12px] pr-[8px] pl-[40px] items-center  ${isDone && "line-through"}`} onClick={toggleDone}> {title} </div>
          <div className="right-0 top-0 py-[12px] px-[16px] hover:bg-slate-700 hover:text-white" onClick={rmItem}>{"\u00D7"}</div>
        </div>
      </li>
    );
  }

  // bogus data that will be pulled from the backend in real life
  // const listUrl = "https://list.sfertman.com/1234567890"
  // localStorage.setItem(listUrl, JSON.stringify(egData));

  // const client = new Client(listUrl);

  const listUrl = "http://localhost:8083/items"
  localStorage.setItem(listUrl, JSON.stringify(egData));

  const client = new Client(listUrl);


  const [items, $setItems] = useState({});

  function setItems(items) {
    let entries = Object.entries(items);
    let grouped = groupBy(([_, {status}]) => status, entries);
    let sorted = mapValues(gr => sortEntriesByKey(gr), grouped);
    let all = Object.fromEntries(catValues(sorted));
    $setItems(all);
    client.setItems(all); //out of band
  }

  useEffect(function fetchItemsEffect() {
    (async function fetchItems() {
      setItems(await client.getItems())
    })();
  }, [])

  function onSubmitForm(e) {
    e.preventDefault(); // need this here to prevent page refresh
    let form = e.target;
    let formData = new FormData(form);
    let newItemTitle = formData.get("new-list-item");
    if (!newItemTitle) return
    setItems({ ...items, [newItemTitle]: { status: STATUS.NEW } });
    form.reset() // deletes the input field
  }
  return (
    <div className="m-[5px] font-serif p-[10px]">
      <div className="bg-slate-500 text-white py-[30px] px-[30px] ">
        <div className="text-center text-5xl p-[10px] py-[20px]">
          My Shopping List
        </div>
        <form className="flex flex-row px-[10px]" onSubmit={onSubmitForm}>
          <input
            id="new-list-item"
            type="text"
            name="new-list-item"
            placeholder="Title..."
            className="m-0 w-3/4 p-[10px] text-base text-slate-950" />
          <button type="Submit" className="p-[10px] w-1/4 bg-slate-300 text-slate-500 align-middle text-center text-base cursor-pointer hover:bg-slate-400 select-none">Add</button>
        </form>
      </div>
      <div>
        <ul>
          {Object.entries(items).map(e => <Item key={Math.random().toString()} item={e} />)}
        </ul>
      </div>
    </div>
  );
}
