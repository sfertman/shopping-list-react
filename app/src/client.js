import axios from "axios";

export class Client {
  constructor(listUrl) {
    this.listUrl = listUrl;
    this.axios = axios.create({ baseURL: listUrl })
  }

  async getItems() {
    let res = await this.axios.get("/");
    return res.data;
  }

  async setItems(items) {
    let res = await this.axios.post("/", items, {
      headers: {"content-type":"application/json"}
    });
    return res.data;
  }
}

export class LocalClient {
  constructor(listUrl) {
    this.listUrl = listUrl;
  }

  async getItems() {
    return Promise.resolve(JSON.parse(localStorage.getItem(this.listUrl)));
  }

  async setItems(items) {
    return Promise.resolve(localStorage.setItem(this.listUrl, JSON.stringify(items)));
  }
}