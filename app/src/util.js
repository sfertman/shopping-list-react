
async function getItems() {
  let res = await fetch({
    method: "GET",
    host: `https://${host}/items`
  });
  if (res.ok) {
    return res.json();
  }
}

async function updateItems(items, fn) {
  let itemsUpdated = fn(items.slice());
  let res = await fetch({
    method: 'PUT',
    host: `https://${host}/items`,
    body: itemsUpdated
  });
  if (!res.ok) {
    return items;
  }
  return itemsUpdated;
}

export function sortBy(keyfn, ...args) {
  // sortBy(keyfn, coll)
  // sortBy(keyfn, comp, coll)
  let comp, coll;
  if (args.length == 1) {
    coll = args[0];
    comp = (a, b) => {
      if (a < b) return -1;
      if (a > b) return 1;
      return 0;
    };
  }
  if (args.length == 2) {
    [comp, coll] = args;
  }
  return coll.sort((a, b) => comp(keyfn(a), keyfn(b)));
}

export function sortByFn(keyfn, comp) {
  // Returns a sortBy function that can be applied on collections.
  // sortBy(keyfn)
  // sortBy(keyfn, comp)
  if (!comp) return coll => sortBy(keyfn, coll);
  return coll => sortBy(keyfn, comp, coll);
}

function applyToEntries(fn, obj) {
  return Object.fromEntries(fn(Object.entries(obj)));
}

export function sortEntriesByKey(entries) {
  return sortBy(([k])=>k.toUpperCase(), entries)
}

export function sortObjectByKey(obj) {
  return applyToEntries(sortByFn(([k])=>k.toUpperCase()), obj);
}

export function groupBy(f, coll) {
  return coll.reduce((ret, x) => {
    let k = f(x);
    if (ret[k]) { ret[k].push(x); }
    else { ret[k] = [x]; }
    return ret;
  }, {});
}

function mapEntries(f, obj) {
  return Object.fromEntries(Object.entries(obj).map(([k, v]) => f(k, v)));
}

/* function mapKeys(fk, obj) { // prolly don't need this
  return mapEntries((k,v) => [fk(k), v], obj);
} */

export function mapValues(fv, obj) {
  return mapEntries((k,v) => [k, fv(v)], obj);
}

export function mergeValues(obj) {
  return Object.entries(obj).reduce((o, [k,v])=>{return {...o, ...v}}, {});
}

export function catValues(obj) {
  return Object.entries(obj).reduce((arr, [k,v])=>arr.concat(v), []);
}

export function dissoc(obj, ...ks) {
  let entries = Object.entries(obj);
  let filtered = entries.filter(([k,_]) => !ks.includes(k))
  return Object.fromEntries(filtered);
}


// TODO: clean up this namespace