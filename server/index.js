import express from "express";
import cors from "cors";
const STATUS = { NEW: 0, DONE: 1 };


let db = {// example data -- this will be fetched from backend
  "Hit the gym": { status: STATUS.NEW },
  "Pay bills": { status: STATUS.DONE },
  "Meet George": { status: STATUS.NEW },
  "Buy eggs": { status: STATUS.NEW },
  "Read a book": { status: STATUS.NEW },
  "Organize office": { status: STATUS.NEW }
};


const corsmw = cors({
  methods: ["GET", "POST"],
  origin: function(o, cb){
      if(!o) return cb(null, true); // allow no origin
      return cb(null, o); // allow incoming origin whatever it is
    },
  credentials: true
})

express().use(corsmw).get("/items", function getItems(req,res) {
  console.log("getting items")
  return res.status(200).json(db);
}).post("/items", express.json(), function postItems(req,res) {
  console.log("req.body",req.body)
  console.log("db", db)
  db = req.body;
  console.log("db after", db)
  return res.status(200).json(db);
}).use((err,req,res,next) => {
  console.error(err);
}).listen(8083, ()=>console.log("Server listening on 8083"));